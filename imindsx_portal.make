; Drupal.org release file.
api = 2
core = 7.x

; Core.
projects[drupal][type] = core
projects[drupal][version] = 7.34
projects[drupal][patch][] = "https://bitbucket.org/iminds_academy/imindsx-portal-drush/raw/3de10f2d093fe47fa6b59b50405d1b3e140488bc/patches/D7-scale-and-crop-1252606-37.patch"

; Install profile.
projects[imindsx_portal_profile][download][branch] = "develop"
projects[imindsx_portal_profile][download][type] = "git"
projects[imindsx_portal_profile][download][url] = "git@bitbucket.org:iminds_academy/imindsx-portal-profile.git"
projects[imindsx_portal_profile][type] = "profile"

; Basic contributed modules.
projects[admin_menu][subdir] = "contrib"
projects[admin_menu][version] = "3.0-rc4"
projects[bean][subdir] = "contrib"
projects[bean][version] = "1.8"
projects[better_exposed_filters][subdir] = "contrib"
projects[better_exposed_filters][version] = "3.0"
projects[browscap][subdir] = "contrib"
projects[browscap][version] = "2.2"
projects[ctools][subdir] = "contrib"
projects[ctools][version] = "1.6"
projects[custom_pub][subdir] = "contrib"
projects[custom_pub][version] = "1.3"
projects[date][subdir] = "contrib"
projects[date][version] = "2.8"
projects[devel][subdir] = "contrib"
projects[devel][version] = "1.5"
projects[draggableviews][subdir] = "contrib"
projects[draggableviews][version] = "2.0"
projects[entity][subdir] = "contrib"
projects[entity][version] = "1.5"
projects[features][subdir] = "contrib"
projects[features][version] = "2.3"
projects[field_collection][subdir] = "contrib"
projects[field_collection][version] = "1.0-beta8"
projects[google_analytics][subdir] = "contrib"
projects[google_analytics][version] = "1.4"
projects[mobile_switch][subdir] = "contrib"
projects[mobile_switch][version] = "2.0-beta1"
projects[mobile_switch][patch][] = "https://bitbucket.org/iminds_academy/drupal-patches/raw/94c4ec3381c4e63e7c6aa6a1e0fc5977474bcfdf/PORTAL-4_mobile_switch_error_on_install.patch"
projects[mobile_switch_panels][subdir] = "contrib"
projects[mobile_switch_panels][version] = "2.0-alpha1"
projects[panels][subdir] = "contrib"
projects[panels][version] = "3.5"
projects[imce][subdir] = "contrib"
projects[imce][version] = "1.9"
projects[imce_wysiwyg][subdir] = "contrib"
projects[imce_wysiwyg][version] = "1.0"
projects[libraries][subdir] = "contrib"
projects[libraries][version] = "2.2"
projects[link][subdir] = "contrib"
projects[link][version] = "1.3"
projects[masquerade][subdir] = "contrib"
projects[masquerade][version] = "1.0-rc7"
projects[pathauto][subdir] = "contrib"
projects[pathauto][version] = "1.2"
projects[strongarm][subdir] = "contrib"
projects[strongarm][version] = "2.0"
projects[token][subdir] = "contrib"
projects[token][version] = "1.5"
projects[views][subdir] = "contrib"
projects[views][version] = "3.10"
projects[views_load_more][subdir] = "contrib"
projects[views_load_more][version] = "1.5"
projects[webform][subdir] = "contrib"
projects[webform][version] = "4.3"
projects[webform_block][subdir] = "contrib"
projects[webform_block][version] = "1.0"
projects[webform_hints][subdir] = "contrib"
projects[webform_hints][version] = "1.5"
projects[webform_share][subdir] = "contrib"
projects[webform_share][version] = "1.2"
projects[wysiwyg][subdir] = "contrib"
projects[wysiwyg][version] = "2.2"
projects[xmlsitemap][subdir] = "contrib"
projects[xmlsitemap][version] = "2.0-rc2"

; Custom modules.
projects[iminds-academy-frontpage][download][branch] = "develop"
projects[iminds-academy-frontpage][download][type] = "git"
projects[iminds-academy-frontpage][download][url] = "git@bitbucket.org:iminds_academy/iminds-academy-frontpage.git"
projects[iminds-academy-frontpage][subdir] = "custom"
projects[iminds-academy-frontpage][type] = "module"
projects[iminds-academy-theme-library][download][branch] = "develop"
projects[iminds-academy-theme-library][download][type] = "git"
projects[iminds-academy-theme-library][download][url] = "git@bitbucket.org:iminds_academy/iminds-academy-theme-library.git"
projects[iminds-academy-theme-library][subdir] = "custom"
projects[iminds-academy-theme-library][type] = "module"
projects[imindsx-portal-edit-links][download][branch] = "develop"
projects[imindsx-portal-edit-links][download][type] = "git"
projects[imindsx-portal-edit-links][download][url] = "git@bitbucket.org:iminds_academy/imindsx-portal-edit-links.git"
projects[imindsx-portal-edit-links][subdir] = "custom"
projects[imindsx-portal-edit-links][type] = "module"
projects[imindsx-portal-node-import][download][branch] = "develop"
projects[imindsx-portal-node-import][download][type] = "git"
projects[imindsx-portal-node-import][download][url] = "git@bitbucket.org:iminds_academy/imindsx-portal-node-import.git"
projects[imindsx-portal-node-import][subdir] = "custom"
projects[imindsx-portal-node-import][type] = "module"
projects[imindsx-portal-course][download][branch] = "develop"
projects[imindsx-portal-course][download][type] = "git"
projects[imindsx-portal-course][download][url] = "git@bitbucket.org:iminds_academy/imindsx-portal-course.git"
projects[imindsx-portal-course][subdir] = "custom"
projects[imindsx-portal-course][type] = "module"
projects[imindsx-portal-main][download][branch] = "develop"
projects[imindsx-portal-main][download][type] = "git"
projects[imindsx-portal-main][download][url] = "git@bitbucket.org:iminds_academy/imindsx-portal-main.git"
projects[imindsx-portal-main][subdir] = "custom"
projects[imindsx-portal-main][type] = "module"
projects[imindsx-portal-partners][download][branch] = "develop"
projects[imindsx-portal-partners][download][type] = "git"
projects[imindsx-portal-partners][download][url] = "git@bitbucket.org:iminds_academy/imindsx-portal-partners.git"
projects[imindsx-portal-partners][subdir] = "custom"
projects[imindsx-portal-partners][type] = "module"

; Themes
projects[omega][version] = 4.3
projects[omega][patch][] = https://bitbucket.org/iminds_academy/drupal-patches/raw/664449175cae7c1874b02ac0692238f058992e3e/PILOT-1024_omega_undefined.patch
projects[iminds_academy_theme][type] = "theme"
projects[iminds_academy_theme][download][type] = "git"
projects[iminds_academy_theme][download][url] = "git@bitbucket.org:iminds_academy/iminds-academy-theme.git"
projects[iminds_academy_theme][download][branch] = "develop"
projects[imindsx-portal-theme][type] = "theme"
projects[imindsx-portal-theme][download][type] = "git"
projects[imindsx-portal-theme][download][url] = "git@bitbucket.org:iminds_academy/imindsx-portal-theme.git"
projects[imindsx-portal-theme][download][branch] = "develop"

; Libraries which are needed for Omega theme(incorporated here because names of Omega .make and Omega project are different and build fails)
libraries[html5shiv][download][type] = "git"
libraries[html5shiv][download][url] = "https://github.com/fubhy/html5shiv.git"
libraries[matchmedia][download][type] = "git"
libraries[matchmedia][download][url] = "https://github.com/fubhy/matchmedia.git"
libraries[pie][download][type] = "git"
libraries[pie][download][url] = "https://github.com/fubhy/pie.git"
libraries[respond][download][type] = "git"
libraries[respond][download][url] = "https://github.com/fubhy/respond.git"
libraries[selectivizr][download][type] = "git"
libraries[selectivizr][download][url] = "https://github.com/fubhy/selectivizr.git"

; Libraries for Mobile Switch
libraries[Mobile_Detect][download][type] = "file"
libraries[Mobile_Detect][download][url] = "https://github.com/serbanghita/Mobile-Detect/archive/2.8.12.zip"

